package com.example.whatever.controllers;

import java.util.List;

import com.example.whatever.dtos.CaregiverDTO;
import com.example.whatever.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/caregiver")
public class CaregiverController {

	@Autowired
	private CaregiverService caregiverService;
	
	// create -- ok
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
		return caregiverService.create(caregiverDTO);
	}
	
	// read - filter by name -- ok
	@RequestMapping(value = "/details/{name}", method = RequestMethod.GET)
	public List<CaregiverDTO> getCaregiverByName(@PathVariable("name") String name) {
		return caregiverService.findCaregiverByName(name);
	}

	// read all -- ok
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<CaregiverDTO> getAllCaregivers() {
		return caregiverService.findAll();
	}

	// update -- ok
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public void updateCaregiver(@PathVariable("id") Integer id, @RequestBody CaregiverDTO caregiverDTO) {		
		caregiverService.update(id, caregiverDTO);
	}
	
	// delete -- ok
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public CaregiverDTO deleteCaregiver(@PathVariable("id") Integer id) {
		return caregiverService.delete(id);
	}
}

