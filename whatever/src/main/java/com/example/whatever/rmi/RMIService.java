package com.example.whatever.rmi;

import com.example.whatever.entities.MedicationPlan;
import com.example.whatever.entities.NotTakenMedicationPlan;
import com.example.whatever.entities.TakenMedicationPlan;
import com.example.whatever.repos.MedicationPlanRepo;
import com.example.whatever.repos.NotTakenMedicationPlanRepo;
import com.example.whatever.repos.TakenMedicationPlanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.rmi.RemoteException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


@Service
public class RMIService implements RMIServiceInterface{

    @Autowired
    private MedicationPlanRepo repo;

    @Autowired
    private TakenMedicationPlanRepo takenRepo;

    @Autowired
    private NotTakenMedicationPlanRepo notTakenRepo;


    // ok
    @Override
    public LocalTime getCurrentTime() {
        // System.out.println("Requested current time");
        return LocalTime.now();
    }

    // ok
    @Override
    public List<MedicationPlan> getTodayMedicationPlan(int id) {
        System.out.println("Requested medication plan");
        List<MedicationPlan> plan = new ArrayList<>();
        for(MedicationPlan m: repo.findAll()){
            if(m.getPatient() == id){
                plan.add(m);
            }
        }
        System.out.println("Offered: " + plan.toString());
        return plan;
    }

    // ok
    @Override
    public void setMedicationTaken(MedicationPlan medicationPlan) throws RemoteException {
        System.out.println("MEDICATION TAKEN: " + medicationPlan.toString());
        // takenRepo.save(new TakenMedicationPlan(medicationPlan.getId(), medicationPlan.getPatient(), medicationPlan.getMedication(), medicationPlan.getInterval(), medicationPlan.getPeriod()));
    }

    // ok
    @Override
    public void setMedicationNotTaken(MedicationPlan medicationPlan) throws RemoteException {
        System.out.println("MEDICATION NOT TAKEN: " + medicationPlan.toString());
        // notTakenRepo.save(new NotTakenMedicationPlan(medicationPlan.getId(), medicationPlan.getPatient(), medicationPlan.getMedication(), medicationPlan.getInterval(), medicationPlan.getPeriod()));
    }
}
