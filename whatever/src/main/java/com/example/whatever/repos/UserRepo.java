package com.example.whatever.repos;

import java.util.Optional;
import com.example.whatever.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepo extends JpaRepository<User, Integer> {

    Optional<User> findByName(String name);

    Optional<User> findById(int id);

    Optional<User> findByUsername(String username);

}