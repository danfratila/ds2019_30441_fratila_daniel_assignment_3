package com.example.whatever.repos;

import java.util.Optional;
import com.example.whatever.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MedicationRepo extends JpaRepository<Medication, Integer> {

    Optional<Medication> findByName(String name);

    Optional<Medication> findById(int id);

}