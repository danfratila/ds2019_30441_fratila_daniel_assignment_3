package com.example.whatever.event;
import com.example.whatever.dtos.PatientDataDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NotificationCreatedEvent extends BaseEvent{

    private PatientDataDTO notification;


    public NotificationCreatedEvent(PatientDataDTO dto) {
        super(EventType.NOTIFICATION_CREATED);
        this.notification = dto;
    }
}
