package com.example.whatever.event;


public class BaseEvent {
    private EventType type;

    public BaseEvent(){

    }

    public BaseEvent(EventType event){
        this.type = event;
    }

    public EventType getType(){
        return this.type;
    }

    public void setType(EventType type){
        this.type = type;
    }
}
