package com.example.whatever.services;

import com.example.whatever.dtos.PatientDataDTO;
import com.example.whatever.dtos.UserDTO;
import com.example.whatever.entities.User;
import com.example.whatever.event.BaseEvent;
import com.example.whatever.event.NotificationCreatedEvent;
import com.example.whatever.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserService {

    @Autowired
    private UserRepo usrRepository;

    @Autowired
    private ReceiverService receiverService;


    // log in
    public UserDTO getSpecifiedUser(String username, String password) throws Exception {
        Optional<User> usr = usrRepository.findByUsername(username);
        UserDTO dto = new UserDTO();
        if (usr.isPresent() && usr.get().getPassword().equals(password)) {
            dto = new UserDTO.Builder()
                    .role(usr.get().getRole())
                    .create();
            receiverService.establishConnection();
        }
        System.out.println("Rol::" + dto.getRole());
        return dto;
    }
}