package com.example.whatever.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.whatever.dtos.CaregiverDTO;
import com.example.whatever.entities.Caregiver;
import com.example.whatever.entities.User;
import com.example.whatever.repos.CaregiverRepo;
import com.example.whatever.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static com.example.whatever.services.UtilityServices.extractNames;
import static com.example.whatever.services.UtilityServices.getNormalDateFromString;


@Service
public class CaregiverService {

	@Autowired
	private UserRepo usrRepository;
	
	@Autowired
	private CaregiverRepo caregiverRepository;

	public CaregiverDTO findCaregiverById(int id) {
		Optional<User> usr = usrRepository.findById(id);
		Optional<Caregiver> caregiver = caregiverRepository.findById(id);
		if (!usr.isPresent() || !caregiver.isPresent()) {
			// throw new ResourceNotFoundException(User.class.getSimpleName());
			return new CaregiverDTO();
		}
		else{
			String[] names = extractNames(usr.get().getName());
			return new CaregiverDTO.Builder()
					.id(usr.get().getId())
					.firstname(names[0])
					.surname(names[1])
					.birthdate(usr.get().getBirthdate())
					.gender(usr.get().getGender())
					.address(usr.get().getAddress())
					//.record(caregiver.getMedicalRecord())
					.create();
		}
	}

	
	public List<CaregiverDTO> findAll() {
		List<Caregiver> caregivers = caregiverRepository.findAll();
		List<CaregiverDTO> toReturn = new ArrayList<>();
		for (Caregiver caregiver: caregivers) {
			Optional<User> user = usrRepository.findById(caregiver.getId());
			if(user.isPresent()){
				String[] names = extractNames(user.get().getName());
				toReturn.add(new CaregiverDTO.Builder()
						.id(user.get().getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.get().getBirthdate())
						.gender(user.get().getGender())
						.address(user.get().getAddress())
						.patients(caregiverRepository.findById(user.get().getId()).get().getPatients()) // de revenit aici si trec caregiver la user nu invers
						.create());
			}
		}
		return toReturn;
	}


	public int create(CaregiverDTO caregiverDTO) {
		User user = new User();
		
		user.setName(caregiverDTO.getFirstname() + " " + caregiverDTO.getSurname());
		Date date = getNormalDateFromString(caregiverDTO.getBirthdate());
		user.setBirthdate(date);
		user.setGender(caregiverDTO.getGender());
		user.setAddress(caregiverDTO.getAddress());
		
		User newUser = usrRepository.save(user);
		
		Caregiver caregiver = new Caregiver();
		caregiver.setId(newUser.getId());
//		caregiver.setMedicalRecord(caregiverDTO.getRecord());
		
		Caregiver ptnt = caregiverRepository.save(caregiver);
		return ptnt.getId();
	}


	public List<CaregiverDTO> findCaregiverByName(String name) {
		// TODO Auto-generated method stub
		List<CaregiverDTO> dtos = new ArrayList<>();
		List<User> users = usrRepository.findAll(); 
		for(User user: users) {
			if(caregiverRepository.findById(user.getId()).isPresent()
					&& user.getName().toLowerCase().contains(name.toLowerCase())) {
				String[] names = extractNames(user.getName());
				CaregiverDTO dto = new CaregiverDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
						//.record(caregiverRepository.findById(user.getId()).getMedicalRecord())
						.create();
				dtos.add(dto);
			}
		}
		return dtos;
	}


	public void update(Integer id, CaregiverDTO caregiverDTO) {
		// TODO Auto-generated method stub
		Optional<User> user = usrRepository.findById(id); // normally same with caregiver dto
		if(user.isPresent()) {
			String[] names = extractNames(user.get().getName());
			Optional<Caregiver> caregiver = caregiverRepository.findById(user.get().getId());
			if(caregiver.isPresent()){
				if(caregiverDTO.getFirstname() != null && !caregiverDTO.getFirstname().equals("")) {
					names[0] = caregiverDTO.getFirstname();
				}
				if(caregiverDTO.getSurname() != null && !caregiverDTO.getSurname().equals("")) {
					names[1] = caregiverDTO.getSurname();
				}
				user.get().setName(names[0] + " " + names[1]);
				if(caregiverDTO.getBirthdate() != null && !caregiverDTO.getBirthdate().equals("")) {
					Date date = getNormalDateFromString(caregiverDTO.getBirthdate());
					user.get().setBirthdate(date);
				}
				if(caregiverDTO.getGender() != null && !caregiverDTO.getGender().equals("")) {
					user.get().setGender(caregiverDTO.getGender());
				}
				if(caregiverDTO.getAddress() != null && !caregiverDTO.getAddress().equals("")) {
					user.get().setAddress(caregiverDTO.getAddress());
				}
				usrRepository.save(user.get());
			}
		}
	}


	public CaregiverDTO delete(Integer id) {
		Optional<User> user = usrRepository.findById(id);
		if(user.isPresent()) {
			usrRepository.delete(user.get());
			caregiverRepository.delete(caregiverRepository.findById(id).get());
		}
		return new CaregiverDTO();
	}
}
