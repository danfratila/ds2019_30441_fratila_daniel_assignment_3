package com.example.whatever.controllers;

import com.example.whatever.entities.MedicationPlan;
import com.example.whatever.rmi.RMIServiceInterface;
import com.example.whatever.view.Gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Controller{

    private List<MedicationPlan> plan;
    private List<MedicationPlan> toBeTakenSoon;
    private List<MedicationPlan> missedNotTaken;

    private Gui gui;
    private RMIServiceInterface rmi;
    private LocalTime currentTime;
    private static final LocalTime DOWNLOAD_TIME = LocalTime.now().plusSeconds(3);//.of(21,20,50);
    private final static int PATIENT_ID = 1;


    public Controller(RMIServiceInterface rmi){
        this.plan = new ArrayList<>();
        this.toBeTakenSoon = new ArrayList<>();
        this.missedNotTaken = new ArrayList<>();
        this.rmi = rmi;
        this.gui = new Gui();
    }

    // Get time
    // -----------------------------------------------------------------------------------------------------------------
    private void requestCurrentTime() {
        currentTime = rmi.getCurrentTime();
        // System.out.println(currentTime.toString());
    }

    private void updateTimerLabel(){
        this.gui.replaceLabelText("Current time: " + this.currentTime.toString());
    }


    // Show plan
    // -----------------------------------------------------------------------------------------------------------------
    private void clearInfoFields(){
        this.gui.clearInfoFields();
    }

    private void createFieldBlank(MedicationPlan medicationPlan){
        this.gui.createRow(medicationPlan.getMedication(), 0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    private void createFieldToBeTaken(MedicationPlan medicationPlan){
        this.gui.createRow(medicationPlan.getMedication(), 1, createActionListener(medicationPlan));
    }

    private void createFieldMissedNotTaken(MedicationPlan medicationPlan){
        this.gui.createRow(medicationPlan.getMedication(), 2, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    private void showMedicationPlan(){
        // System.out.println("Ajung in medication plan show -------------");
        this.clearInfoFields();
        for(MedicationPlan m: this.plan) {
            createFieldBlank(m);
        }
        for(MedicationPlan m: this.toBeTakenSoon) {
            createFieldToBeTaken(m);
        }
        for(MedicationPlan m: this.missedNotTaken) {
            createFieldMissedNotTaken(m);
        }
    }

    // Download plan
    // -----------------------------------------------------------------------------------------------------------------
    private boolean isTimeToDownload(){
        boolean isPerfectTime = (this.currentTime.getHour() == DOWNLOAD_TIME.getHour() &&
                this.currentTime.getMinute() == DOWNLOAD_TIME.getMinute() &&
                this.currentTime.getSecond() == DOWNLOAD_TIME.getSecond());
        if(isPerfectTime){
            System.out.println("PERFECT TIME:" + ": " + this.currentTime + "---" + DOWNLOAD_TIME);
        }
        else{
            // System.out.println("NOP TIME:" + this.currentTime + "---" + DOWNLOAD_TIME);
        }
        return isPerfectTime;
    }

    private void downloadPlan(){
        plan = rmi.getTodayMedicationPlan(PATIENT_ID);
        showMedicationPlan();
    }


    // Set medication taken
    // -----------------------------------------------------------------------------------------------------------------
    // what happens on each tick
    private void checkTime(){
        boolean changeAppeared = false;
        // check if it time to take any medication
        for(MedicationPlan medicationPlan: plan){
            if(isTimeToTakeMedication(medicationPlan)){
                System.out.println("Change appeared, must be taken: " + medicationPlan.toString());
                changeAppeared = true;
                toBeTakenSoon.add(medicationPlan);
            }
        }
        // remove them from the actual plan
        for(MedicationPlan medicationPlan: toBeTakenSoon) {
            if(plan.contains(medicationPlan)){
                plan.remove(medicationPlan);
            }
        }


        // check if time passed for any medication to be taken
        for(MedicationPlan medicationPlan: toBeTakenSoon){
            if(passedTimeToTakeMedication(medicationPlan)){
                System.out.println("Change appeared, passed time to be taken: " + medicationPlan.toString());
                changeAppeared = true;
                missedNotTaken.add(medicationPlan);
                notTakenMedication(medicationPlan);
            }
        }
        // remove them from the to be taken soon
        for(MedicationPlan medicationPlan: missedNotTaken) {
            if(toBeTakenSoon.contains(medicationPlan)){
                toBeTakenSoon.remove(medicationPlan);
            }
        }

        // repaint
        if(changeAppeared){
            System.out.println("Change appeared, repainting");
            showMedicationPlan();
        }
    }

    private boolean isTimeToTakeMedication(MedicationPlan medicationPlan){
        boolean isTime = false;
        // get current time (to minutes), see if it coincides with interval to take
        if((currentTime.getHour() * 60 + currentTime.getMinute()) % medicationPlan.getInterval() == 0){
            isTime = true;
        }
        return isTime;
    }


    private boolean passedTimeToTakeMedication(MedicationPlan medicationPlan){
        boolean isTimePassed = false;
        // get current time (to minutes), see if it coincides with interval to take
        // you have one minute to take the medication
        if((currentTime.getHour() * 60 + currentTime.getMinute()) % medicationPlan.getInterval() != 0){
            isTimePassed = true;
        }
        return isTimePassed;
    }



    private void takeMedication(MedicationPlan medicationPlan){
        try {
            rmi.setMedicationTaken(medicationPlan);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        this.toBeTakenSoon.remove(medicationPlan);
        showMedicationPlan();
    }

    private void notTakenMedication(MedicationPlan medicationPlan){
        try {
            rmi.setMedicationNotTaken(medicationPlan);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private ActionListener createActionListener(MedicationPlan medicationPlan){
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // System.out.println("AM APASAT: " + medicationPlan.toString());
                takeMedication(medicationPlan);
            }
        };
    }


    // -----------------------------------------------------------------------------------------------------------------
    private void waitFunction(){
        try {
            wait(990);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    // -----------------------------------------------------------------------------------------------------------------
    public void run() {
        while(true){
            synchronized(this){
                requestCurrentTime();
                updateTimerLabel();
                if(isTimeToDownload()){
                    downloadPlan();
                }
                checkTime();
                waitFunction();
            }
        }
    }
}
